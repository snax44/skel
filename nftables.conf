#!/usr/sbin/nft -f
define home_ip = 1.2.3.4
define work_ip = { 2.3.4.5/23, 3.4.5.6/16 }
define trusted = { $home_ip, $work_ip }
define ntp_ip = { 51.145.123.29, 51.178.43.227, 45.83.234.123, 91.121.154.174 }

define loopback_int = lo
define wan_int = ens18

define http_port = 80
define https_port = 443
define dns_port = 53
define ssh_port = 22
define smtp_port = 465
define whois_port = 43
define ntp_port = 123

flush ruleset

table ip filter {
  chain chain_in {
    type filter hook input priority filter; policy drop;
    ct state related,established accept
    iifname $loopback_int accept
    iifname $wan_int jump wan_to_local
  }

  chain wan_to_local {
    udp dport $wg0_port accept
    ip saddr $trusted jump trusted_to_local
  }

  chain trusted_to_local {
    ip saddr $trusted icmp type echo-request accept
    ip saddr $trusted tcp dport $ssh_port accept
  }

  chain chain_fw {
    type filter hook forward priority filter; policy drop;
    ct state related,established accept
  }
  
  chain chain_out {
    type filter hook output priority filter; policy accept;
    ct state related,established accept
    oifname $loopback_int accept
    oifname $wan_int jump local_to_wan
  }
  
  chain local_to_wan {
    ip protocol tcp jump local_to_wan_tcp
    ip protocol udp jump local_to_wan_udp
  }

  chain local_to_wan_tcp {
    tcp dport $http_port accept
    tcp dport $https_port accept
    tcp dport $ssh_port accept
    tcp dport $dns_port accept
    tcp dport $smtp_port accept
    tcp dport $whois_port accept
  }
  chain local_to_wan_udp {
    udp dport $dns_port accept
    udp dport $ntp_port ip daddr $ntp_ip accept;
  }
}

table ip6 filter {
  chain chain_in {
    type filter hook input priority filter; policy drop;
    ct state established,related accept
    iifname $loopback_int accept
    iifname $wan_int jump wan_to_local_6
  }

  chain wan_to_local_6 {
    ip6 nexthdr icmpv6 accept
  }

  chain chain_fw {
    type filter hook forward priority filter; policy drop;
    ct state established,related accept
  }

  chain chain_out {
    type filter hook output priority filter; policy drop;
    ct state established,related accept
    oifname $loopback_int accept
    oifname $wan_int jump local_to_wan_6
  }

  chain local_wan_6 {
    ip6 nexthdr icmpv6 accept
    tcp dport $http_port accept
    tcp dport $https_port accept
    tcp dport $ssh_port accept
    tcp dport $dns_port accept
    tcp dport $smtp_port accept
  }
}

table ip nat {
  chain chain_prerouting {
    type nat hook prerouting priority filter; policy accept;
  }
  
  chain chain_postrouting {
    type nat hook postrouting priority filter; policy accept;
  }
}

table ip6 nat {
  chain chain_prerouting {
    type nat hook prerouting priority filter; policy accept;
  }
  
  chain chain_postrouting {
    type nat hook postrouting priority filter; policy accept;
  }
}
